--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:41:05 01/21/2017
-- Design Name:   
-- Module Name:   D:/git_repos/datapath_fpga/test_insReg2.vhd
-- Project Name:  RutaDeDatos
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: instruction_reg
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_insReg2 IS
END test_insReg2;
 
ARCHITECTURE behavior OF test_insReg2 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT instruction_reg
    PORT(
         ERI : IN  std_logic;
         clk : IN  std_logic;
			reset : IN  std_logic;
         input : IN  std_logic_vector(15 downto 0);
         op : OUT  std_logic_vector(1 downto 0);
         addr_src : OUT  std_logic_vector(6 downto 0);
         addr_dst : OUT  std_logic_vector(6 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal ERI : std_logic := '0';
   signal clk : std_logic := '0';
	signal reset : std_logic := '0';
   signal input : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal op : std_logic_vector(1 downto 0);
   signal addr_src : std_logic_vector(6 downto 0);
   signal addr_dst : std_logic_vector(6 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: instruction_reg PORT MAP (
          ERI => ERI,
          clk => clk,
			 reset => reset,
          input => input,
          op => op,
          addr_src => addr_src,
          addr_dst => addr_dst
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- Enable the registry
      ERI <= '1';
		wait for clk_period;
		
		-- Test 1
		input <= x"0000";
		wait for clk_period;
		assert op = "00" report "Error test 1.1" severity warning;
		assert addr_src = "0000000" report "Error test 1.2" severity warning;
		assert addr_dst = "0000000" report "Error test 1.3" severity warning;
		
		-- Test 2
		input <= x"FFFF";
		wait for clk_period;
		assert op = "11" report "Error test 2.1" severity warning;
		assert addr_src = "1111111" report "Error test 2.2" severity warning;
		assert addr_dst = "1111111" report "Error test 2.3" severity warning;
		
		-- Test 3
		input <= "1000010101010000";  -- 10 0001010 1010000
		wait for clk_period;
		assert op = "10" report "Error test 3.1" severity warning;
		assert addr_src = "0001010" report "Error test 3.2" severity warning;
		assert addr_dst = "1010000" report "Error test 3.3" severity warning;
		
		
		-- Test 4 - disable signal works
		-- Reset input
		input <= X"0000";
		wait for clk_period;
		-- Disable the registry
		ERI <= '0';
		wait for clk_period;
		assert op = "00" report "Error test 4.1" severity warning;
		assert addr_src = "0000000" report "Error test 4.2" severity warning;
		assert addr_dst = "0000000" report "Error test 4.3" severity warning;
		
		input <= x"FFFF";
		wait for clk_period;
		assert op = "00" report "Error test 4.4" severity warning;
		assert addr_src = "0000000" report "Error test 4.5" severity warning;
		assert addr_dst = "0000000" report "Error test 4.6" severity warning;
		
		input <= "1000010101010000";  -- 10 0001010 1010000  
		wait for clk_period;
		assert op = "00" report "Error test 4.7" severity warning;
		assert addr_src = "0000000" report "Error test 4.8" severity warning;
		assert addr_dst = "0000000" report "Error test 4.9" severity warning;
		
		-- Test 5
		reset<='1';
		wait for clk_period;
		assert op = "00" report "Error test 5.1" severity warning;
		assert addr_src = "0000000" report "Error test 5.2" severity warning;
		assert addr_dst = "0000000" report "Error test 5.3" severity warning;

      wait;
   end process;

END;
