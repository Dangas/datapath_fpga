
-- VHDL Instantiation Created from source file Adder.vhd -- 15:50:31 12/25/2016
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT Adder
	PORT(
		i0 : IN std_logic_vector(15 downto 0);
		i1 : IN std_logic_vector(15 downto 0);          
		o0 : OUT std_logic_vector(15 downto 0)
		);
	END COMPONENT;

	Inst_Adder: Adder PORT MAP(
		i0 => ,
		i1 => ,
		o0 => 
	);


