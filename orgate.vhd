----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:38:14 12/26/2016 
-- Design Name: 
-- Module Name:    orgate - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity orgate is
    Port ( or_i0 : in  STD_LOGIC_VECTOR (15 downto 0);
           or_o0 : out  STD_LOGIC );
end orgate;

architecture Behavioral of orgate is
begin
	or_o0 <= or_i0(0) or or_i0(1) or or_i0(2) or
		or_i0(3) or or_i0(4) or or_i0(5) or
		or_i0(6) or or_i0(7) or or_i0(8) or
		or_i0(9) or or_i0(10) or or_i0(11) or
		or_i0(12) or or_i0(13) or or_i0(14) or
		or_i0(15);
end Behavioral;

