--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:44:10 12/29/2016
-- Design Name:   
-- Module Name:   C:/Users/Dan/Desktop/SSEE/proyecto/RutaDeDatos/testXorGate.vhd
-- Project Name:  RutaDeDatos
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: xorgate
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY testXorGate IS
END testXorGate;
 
ARCHITECTURE behavior OF testXorGate IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT xorgate
    PORT(
         i0 : IN  std_logic_vector(15 downto 0);
         i1 : IN  std_logic_vector(15 downto 0);
         o0 : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal i0 : std_logic_vector(15 downto 0) := (others => '0');
   signal i1 : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal o0 : std_logic_vector(15 downto 0);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: xorgate PORT MAP (
          i0 => i0,
          i1 => i1,
          o0 => o0
        );

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
      -- insert stimulus here 
		i0 <= std_logic_vector(to_unsigned(0, 16));
		i1 <= std_logic_vector(to_unsigned(1, 16));
		wait for 10 ns;
		i0 <= std_logic_vector(to_unsigned(0, 16));
		i1 <= std_logic_vector(to_unsigned(0, 16));
      wait;
   end process;

END;
