----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:25:00 12/29/2016 
-- Design Name: 
-- Module Name:    CP - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CP is
    Port	( reset : in  STD_LOGIC;
			  INCP : in  STD_LOGIC;
           E : in  STD_LOGIC_VECTOR (6 downto 0);
           S : out  STD_LOGIC_VECTOR (6 downto 0);
           clock : in  STD_LOGIC);
end CP;

architecture Behavioral of CP is
signal tmp: std_logic_vector(6 downto 0);
begin
	process(reset,clock)
		begin
			if(reset = '1') then
				tmp<=(others=>'0');
			elsif (rising_edge(clock)) then
				if(INCP = '0')	then
					tmp<=E;
				else
					tmp<=E+1;
			end if;
			end if;
	end process;
	S<=tmp;
end Behavioral;