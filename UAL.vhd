----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:01:58 12/25/2016 
-- Design Name: 
-- Module Name:    UAL - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
-- asdasdasdasdasdasdss
entity UAL is
    Port ( A : in  STD_LOGIC_VECTOR (15 downto 0);
           B : in  STD_LOGIC_VECTOR (15 downto 0);
			  UAL0 : in STD_LOGIC_VECTOR (1 downto 0);
			  CLK : in STD_LOGIC;
           Z : out  STD_LOGIC;
           S : out  STD_LOGIC_VECTOR (15 downto 0));
end UAL;



architecture flow of UAL is
	component Mux4a2 is
    Port ( d0 : in  STD_LOGIC_VECTOR (15 downto 0);
           d1 : in  STD_LOGIC_VECTOR (15 downto 0);
           d2 : in  STD_LOGIC_VECTOR (15 downto 0);
           d3 : in  STD_LOGIC_VECTOR (15 downto 0);
           s0 : in  STD_LOGIC_VECTOR (1 downto 0);
           o0 : out  STD_LOGIC_VECTOR (15 downto 0));
	end component Mux4a2;

	component AdderUnsigned16bits is
		Port ( A : in STD_LOGIC_VECTOR (15 downto 0);
				 B : in STD_LOGIC_VECTOR (15 downto 0);
				 CLK : in STD_LOGIC;
				 S : out STD_LOGIC_VECTOR (15 downto 0));
	end component AdderUnsigned16bits;

	component orgate is
		Port ( or_i0 : in STD_LOGIC_VECTOR (15 downto 0);
			    or_o0 : out STD_LOGIC);
	end component orgate;
	
	component xorgate is
		Port ( i0 : in STD_LOGIC_VECTOR (15 downto 0);
			    i1 : in STD_LOGIC_VECTOR (15 downto 0);
			    o0 : out STD_LOGIC_VECTOR (15 downto 0));
	end component xorgate;
	
	signal adder2mux : STD_LOGIC_VECTOR (15 downto 0);
	signal xor2mux : STD_LOGIC_VECTOR (15 downto 0);
	signal B2mux : STD_LOGIC_VECTOR (15 downto 0);
	signal mux2out : STD_LOGIC_VECTOR (15 downto 0);
	signal zDec2Z : STD_LOGIC;
begin
	
	adder : AdderUnsigned16bits port map (
		A => A,
		B => B,
		S => adder2mux,
		CLK => CLK
	);
	
	comparator : xorgate port map (
		i0 => A,
		i1 => B,
		o0 => xor2mux
	);
	
	mux : Mux4a2 port map (
		d0 => adder2mux,
		d1 => xor2mux,
		d2 => B,
		d3 => std_logic_vector(to_unsigned(0, 16)),
		s0 => UAL0,
		o0 => mux2out
	);
	
	zeroDetector : orgate port map (
		or_i0 => mux2out,
		or_o0 => zDec2Z
	);
	
	z <= zDec2Z;
	S <= mux2out;
end flow;

