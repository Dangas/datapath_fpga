
-- VHDL Instantiation Created from source file Mux4a2.vhd -- 15:30:10 12/25/2016
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT Mux4a2
	PORT(
		d0 : IN std_logic_vector(16 downto 0);
		d1 : IN std_logic_vector(16 downto 0);
		d2 : IN std_logic_vector(16 downto 0);
		d3 : IN std_logic_vector(16 downto 0);
		s0 : IN std_logic_vector(2 downto 0);          
		o0 : OUT std_logic_vector(16 downto 0)
		);
	END COMPONENT;

	Inst_Mux4a2: Mux4a2 PORT MAP(
		d0 => ,
		d1 => ,
		d2 => ,
		d3 => ,
		s0 => ,
		o0 => 
	);


