-----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:46:37 01/07/2017 
-- Design Name: 
-- Module Name:    instruction_reg - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity instruction_reg is
    Port ( ERI : in  STD_LOGIC;
			  clk : in  STD_LOGIC;
			  reset : in  STD_LOGIC;
           input : in  STD_LOGIC_VECTOR (15 downto 0);
           op : out  STD_LOGIC_VECTOR (1 downto 0);
           addr_src : out  STD_LOGIC_VECTOR (6 downto 0);
           addr_dst : out  STD_LOGIC_VECTOR (6 downto 0));
end instruction_reg;

architecture Behavioral of instruction_reg is
begin
process (ERI,input,clk)
begin

	if(reset = '1') then
				op<=(others=>'0');
				addr_src<=(others=>'0');
				addr_dst<=(others=>'0');
	
 elsif clk = '1' and clk'event then
	if ERI = '1' then 
		op(1) <=  input (15);
		op(0) <=  input (14);
		addr_src(6) <=  input (13);
		addr_src(5) <=  input (12);
		addr_src(4) <=  input (11);
		addr_src(3) <=  input (10);
		addr_src(2) <=  input (9);
		addr_src(1) <=  input (8);
		addr_src(0) <=  input (7);
		addr_dst(6) <=  input (6);
		addr_dst(5) <=  input (5);
		addr_dst(4) <=  input (4);
		addr_dst(3) <=  input (3);
		addr_dst(2) <=  input (2);
		addr_dst(1) <=  input (1);
		addr_dst(0) <=  input (0);
	end if;
 end if;

end process;

end Behavioral;

