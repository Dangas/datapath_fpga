-- TestBench Template 

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

ENTITY testbench IS
END testbench;

architecture behavior of testbench IS

	-- ALU's number of bits
	constant UALInOutLength : integer := 16;
	
	-- ALU's select input's size
	constant UALSelectLength : integer := 2;
	
	-- The ALU to be tested
	component UAL
	port(				
		A : in  STD_LOGIC_VECTOR (UALInOutLength - 1 downto 0);
		B : in  STD_LOGIC_VECTOR (UALInOutLength - 1 downto 0);
		UAL0 : in STD_LOGIC_VECTOR (UALSelectLength - 1 downto 0);
		CLK : in STD_LOGIC;
		Z : out  STD_LOGIC;
		S : out  STD_LOGIC_VECTOR (UALInOutLength - 1 downto 0));
	end component;    
	
	-- Input signals:
	signal clock_signal : STD_LOGIC := '0';
	
	signal sA, sB : STD_LOGIC_VECTOR(UALInOutLength - 1 downto 0) :=
		STD_LOGIC_VECTOR(to_unsigned(0, UALInOutLength));
		
	signal sUAL0 : STD_LOGIC_VECTOR(UALSelectLength - 1 downto 0) := 
		STD_LOGIC_VECTOR(to_unsigned(0, UALSelectLength));
	
	
	-- Output signals:
	signal sZ : STD_LOGIC;
	signal sS : STD_LOGIC_VECTOR(UALInOutLength - 1 downto 0);

begin
	
	uut : UAL port map (
		A => sA,
		B => sB,
		UAL0 => sUAL0,
		CLK => clock_signal,
		Z => sZ,
		S => sS
	);
		
--  Test Bench Statements
	clock : process
	begin
		clock_signal <= '1';
		wait for 10 ns;
		clock_signal <= '0';
		wait for 10 ns;
	end process;
	
	process
	begin
		-- ALU mode adder
		sUAL0 <= STD_LOGIC_VECTOR(to_unsigned(0, UALSelectLength));
		wait for 50 ns;
		
		-- Test adder 1
		sA <= STD_LOGIC_VECTOR(to_unsigned(10, UALInOutLength));
		sB <= STD_LOGIC_VECTOR(to_unsigned(10, UALInOutLength));
		wait for 50 ns; 
		assert sS = 20 report "Error test adder 1" severity warning;
		assert sZ = '1' report "Error test adder 1 zero flag" severity warning;
		
		-- Test adder 2
		sA <= STD_LOGIC_VECTOR(to_unsigned(0, UALInOutLength));
		sB <= STD_LOGIC_VECTOR(to_unsigned(0, UALInOutLength));
		wait for 50 ns;
		assert sS = 0 report "Error test adder 2" severity warning;
		assert sZ = '0' report "Error test adder 2 zero flag" severity warning;
		
		-- Test adder 2
		sA <= STD_LOGIC_VECTOR(to_unsigned(30000, UALInOutLength));
		sB <= STD_LOGIC_VECTOR(to_unsigned(30000, UALInOutLength));
		wait for 50 ns;
		assert sS = 60000 report "Error test adder 3" severity warning;
		assert sZ = '1' report "Error test adder 3 zero flag" severity warning;


		-- ALU mode comparator
		sUAL0 <= STD_LOGIC_VECTOR(to_unsigned(1, UALSelectLength));
		wait for 50 ns;
		
		-- Test comparator 1
		sA <= STD_LOGIC_VECTOR(to_unsigned(1, UALInOutLength));
		sB <= STD_LOGIC_VECTOR(to_unsigned(1, UALInOutLength));
		wait for 50 ns;
		assert sS = 0 report "Error test comparator 1" severity warning;
		assert sZ = '0' report "Error test comparator 1 zero flag" severity warning;
		
		-- Test comparator 2
		sA <= STD_LOGIC_VECTOR(to_unsigned(10, UALInOutLength));
		sB <= STD_LOGIC_VECTOR(to_unsigned(20, UALInOutLength));
		wait for 50 ns;
		assert sS /= 0 report "Error test comparator 2" severity warning;
		assert sZ = '1' report "Error test comparator 2 zero flag" severity warning;
		
		-- Test comparator 3
		sA <= STD_LOGIC_VECTOR(to_unsigned(2, UALInOutLength));
		sB <= STD_LOGIC_VECTOR(to_unsigned(1, UALInOutLength));
		wait for 50 ns;
		assert sS /= 0 report "Error test comparator 3" severity warning;
		assert sZ = '1' report "Error test comparator 3 zero flag" severity warning;
		
		-- Test comparator 4
		sA <= STD_LOGIC_VECTOR(to_unsigned(20, UALInOutLength));
		sB <= STD_LOGIC_VECTOR(to_unsigned(20, UALInOutLength));
		wait for 50 ns;
		assert sS = 0 report "Error test comparator 4" severity warning;
		assert sZ = '0' report "Error test comparator 4 zero flag" severity warning;
		
		
		-- ALU mode bridge
		sUAL0 <= STD_LOGIC_VECTOR(to_unsigned(2#10#, UALSelectLength));
		wait for 50 ns;
		
		-- Test bridge 1
		sA <= STD_LOGIC_VECTOR(to_unsigned(20, UALInOutLength));
		sB <= STD_LOGIC_VECTOR(to_unsigned(1, UALInOutLength));
		wait for 50 ns;
		assert sS = 1 report "Error test bridge 1" severity warning;
		assert sZ = '1' report "Error test bridge 1 zero flag" severity warning;
		
		-- Test bridge 2
		sA <= STD_LOGIC_VECTOR(to_unsigned(20, UALInOutLength));
		sB <= STD_LOGIC_VECTOR(to_unsigned(20, UALInOutLength));
		wait for 50 ns;
		assert sS = 20 report "Error test bridge 2" severity warning;
		assert sZ = '1' report "Error test bridge 2 zero flag" severity warning;
		
		-- Test bridge 3
		sA <= STD_LOGIC_VECTOR(to_unsigned(0, UALInOutLength));
		sB <= STD_LOGIC_VECTOR(to_unsigned(0, UALInOutLength));
		wait for 50 ns;
		assert sS = 0 report "Error test bridge 3" severity warning;
		assert sZ = '0' report "Error test bridge 3 zero flag" severity warning;
		
		wait;
	end process tb;
	--  End Test Bench 

END;
