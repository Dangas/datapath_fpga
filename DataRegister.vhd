----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:50:54 01/14/2017 
-- Design Name: 
-- Module Name:    DataRegister - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DataRegister is
    Port ( reset : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           cs : in  STD_LOGIC;
			  inputMM : in  STD_LOGIC_VECTOR (15 downto 0);
           outputALU : out  STD_LOGIC_VECTOR (15 downto 0) );
end DataRegister;



architecture Behavioral of DataRegister is

	signal Q: std_logic_vector(15 downto 0) := (others => '0');

begin

	process (clk, reset)
	begin
		if reset = '1' then
			Q <= "0000000000000000";
		elsif (clk = '1' and clk'event) then
			if cs = '1' then
				Q <= inputMM;
			end if;
		end if;
	end process;
	
	outputALU <= Q;

end Behavioral;

