--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   20:20:15 01/20/2017
-- Design Name:   
-- Module Name:   D:/git_repos/datapath_fpga/test_DataRegister.vhd
-- Project Name:  RutaDeDatos
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: DataRegister
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_DataRegister IS
END test_DataRegister;
 
ARCHITECTURE behavior OF test_DataRegister IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT DataRegister
    PORT(
         reset : IN  std_logic;
         clk : IN  std_logic;
         cs : IN  std_logic;
         inputMM : IN  std_logic_vector(15 downto 0);
         outputALU : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal reset : std_logic := '0';
   signal clk : std_logic := '0';
   signal cs : std_logic := '0';
   signal inputMM : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal outputALU : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: DataRegister PORT MAP (
          reset => reset,
          clk => clk,
          cs => cs,
          inputMM => inputMM,
          outputALU => outputALU
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- Set the register
		cs <= '1';
		wait for clk_period;
		
		-- Test datareg 1: value is sent to the output
		inputMM <= X"FFFF";
		wait for clk_period;
		assert outputALU = X"FFFF" report "Error test datareg 1.1" severity warning;
		
		inputMM <= X"0000";
		wait for clk_period;
		assert outputALU = X"0000" report "Error test datareg 1.2" severity warning;
		
		
		-- Test datareg 2: reg. holds the input
		inputMM <= X"FFFF";
		wait for clk_period ;
		assert outputALU = X"FFFF" report "Error test datareg 2.1" severity warning;
		
		wait for clk_period;
		assert outputALU = X"FFFF" report "Error test datareg 2.2" severity warning;
		
		
		-- Test datareg 3: reset works
		reset <= '1';
		wait for clk_period;
		assert outputALU = X"0000" report "Error test datareg 3.1" severity warning;
		-- Disable the reset
		reset <= '0';
		wait for clk_period;
		
		
		-- Test datareg 4: holds value when disabled
		inputMM <= X"FFFF";
		wait for clk_period ;
		cs <= '0';
		wait for clk_period ;
		assert outputALU = X"FFFF" report "Error test datareg 4" severity warning;
		
		
		-- Test datareg 5: does not change value when disabled
		
		-- Precondition: the current value is 0xFFFF
		assert outputALU = X"FFFF" report "Error test datareg 5 pre-condition" severity warning;
		
		inputMM <= X"0000";
		wait for clk_period ;
		assert outputALU = X"FFFF" report "Error test datareg 5" severity warning;
		
      wait;
   end process;

END;
