--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   18:38:45 01/10/2017
-- Design Name:   
-- Module Name:   C:/Users/Diego/Downloads/datapath/CPtest.vhd
-- Project Name:  RutaDeDatos
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: CP
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_cp IS
END test_cp;
 
ARCHITECTURE behavior OF test_cp IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT CP
    PORT(
         reset : IN  std_logic;
         INCP : IN  std_logic;
         E : IN  std_logic_vector(6 downto 0);
         S : OUT  std_logic_vector(6 downto 0);
         clock : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal reset : std_logic := '0';
   signal INCP : std_logic := '1';
   signal E : std_logic_vector(6 downto 0) := (others => '0');
   signal clock : std_logic := '0';
	
 	--Outputs
   signal S : std_logic_vector(6 downto 0);
	

   -- Clock period definitions
   constant clock_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: CP PORT MAP (
          reset => reset,
          INCP => INCP,
          E => E,
          S => S,
          clock => clock
        );

   -- Clock process definitions
   clock_process :process
   begin
		clock <= '0';
		wait for clock_period/2;
		clock <= '1';
		wait for clock_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		wait for 10 ns;
		
      wait for clock_period*10;
	
      --Caso 1: INCP y reset desactivado
		reset<='0';
		INCP<='0';
		wait for 50 ns;
		assert S = E report "Error case 1" severity warning;
		
		--Caso 2: Reset desactivado e INCP activado
		INCP<='1';
		wait for 50 ns;
		assert S = E+1 report "Error case 2" severity warning;
		
		--Caso 3: Reset activado e INCP activado
		reset<='1';
		wait for 50 ns;
		assert S = 0 report "Error case 3" severity warning;
		
		--Caso 4: Reset activado e INCP desactivado
		INCP<='0';
		wait for 50 ns;
		assert S = 0 report "Error case 4" severity warning; 

      wait;
   end process;

END;
