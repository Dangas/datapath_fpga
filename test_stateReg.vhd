--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:29:55 12/31/2016
-- Design Name:   
-- Module Name:   C:/Users/Dan/Desktop/SSEE/proyecto/RutaDeDatos/test_stateReg.vhd
-- Project Name:  RutaDeDatos
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: StateReg
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_stateReg IS
END test_stateReg;
 
ARCHITECTURE behavior OF test_stateReg IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT StateReg
    PORT(i0 : IN  std_logic;
         s0 : IN  std_logic;
         clk : IN  std_logic;
         o0 : INOUT  std_logic);
    END COMPONENT;
    

   --Inputs
   signal i0 : std_logic := '0';
   signal s0 : std_logic := '0';
   signal clk : std_logic := '0';

	--BiDirs
   signal o0 : std_logic;

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: StateReg PORT MAP (
          i0 => i0,
          s0 => s0,
          clk => clk,
          o0 => o0
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

		-- Enable the registry
      s0 <= '1';
		wait for clk_period;
		
		-- Test 1
		i0 <= '1';
		wait for clk_period;
		assert o0 = '1' report "Error test 1" severity warning;
		
		-- Test 2
		i0 <= '0';
		wait for clk_period;
		assert o0 = '0' report "Error test 2" severity warning;
		
		
		-- Reset the input and disable the registry
		i0 <= '0';
		wait for clk_period;
		s0 <= '0';
		wait for clk_period;
		
		-- Test 3
		i0 <= '1';
		wait for clk_period;
		assert o0 = '0' report "Error test 3" severity warning;
		
		-- Test 4
		i0 <= '0';
		wait for clk_period;
		assert o0 = '0' report "Error test 4" severity warning;
		
      wait;
   end process;

END;
