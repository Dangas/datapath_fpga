----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:52:06 12/31/2016 
-- Design Name: 
-- Module Name:    StateReg - Flow 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- Async. latches could produce timing problems in FPGAs.
-- In order to avoid them, this entity uses a clk signal, so it is a flip flop. 
entity StateReg is
    Port ( i0 : in  STD_LOGIC;
           s0 : in  STD_LOGIC;
			  clk : in STD_LOGIC;
           o0 : inout  STD_LOGIC);
end StateReg;

architecture Flow of StateReg is
begin
	o0 <= i0 when s0 = '1' and clk = '1' and clk'event else o0;
end Flow;