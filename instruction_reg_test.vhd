-- TestBench Template 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

  ENTITY instruction_reg_test IS
  END instruction_reg_test;

  ARCHITECTURE behavior OF instruction_reg_test IS 

  -- Component Declaration
          COMPONENT instruction_reg
          PORT(
				  ERI : in  STD_LOGIC;
				  clk : in  STD_LOGIC;
				  input : in  STD_LOGIC_VECTOR (15 downto 0);
				  op : out  STD_LOGIC_VECTOR (1 downto 0);
				  addr_src : out  STD_LOGIC_VECTOR (6 downto 0);
				  addr_dst : out  STD_LOGIC_VECTOR (6 downto 0)
				  );
          END COMPONENT;

          SIGNAL ERI : STD_LOGIC := '0';
          SIGNAL clk : STD_LOGIC := '0';
			 SIGNAL input : STD_LOGIC_VECTOR (15 downto 0) := x"00fe";
			 SIGNAL op : STD_LOGIC_VECTOR (1 downto 0);
			 SIGNAL addr_src : STD_LOGIC_VECTOR (6 downto 0);
			 SIGNAL addr_dst : STD_LOGIC_VECTOR (6 downto 0);
      
			 constant clk_period : time := 20 ns;
    
  BEGIN

  -- Component Instantiation
          uut: instruction_reg PORT MAP(
                  ERI => ERI, 
						clk => clk,
                  input => input,
						op => op,
						addr_src => addr_src,
						addr_dst => addr_dst
				);


  --  Test Bench Statements
     -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

		-- Enable the registry
      ERI <= '1';
		wait for clk_period;
		
		-- Test 1
		input <= x"0000";
		wait for clk_period;
		assert op = "00" report "Error test 1.1" severity warning;
		assert addr_src = "0000000" report "Error test 1.2" severity warning;
		assert addr_dst = "0000000" report "Error test 1.3" severity warning;
		
		-- Test 2
		input <= x"FFFF";
		wait for clk_period;
		assert op = "11" report "Error test 2.1" severity warning;
		assert addr_src = "1111111" report "Error test 2.2" severity warning;
		assert addr_dst = "1111111" report "Error test 2.3" severity warning;
		
		wait;
   end process;

END;

