----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:14:08 01/20/2017 
-- Design Name: 
-- Module Name:    test2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity test2 is
    Port ( i0 : in  STD_LOGIC_VECTOR (15 downto 0);
           clk : in  STD_LOGIC;
           s0 : inout  STD_LOGIC_VECTOR (7 downto 0);
           s1 : inout  STD_LOGIC_VECTOR (7 downto 0));
end test2;

architecture Behavioral of test2 is
begin
	s0 <= i0(0 to 7) when clk = '1' and clk'event else s0;
	s1 <= i0(8 to 15) when clk = '1' and clk'event else s1;
end Behavioral;

