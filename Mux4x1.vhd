----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:29:39 01/03/2017 
-- Design Name: 
-- Module Name:    Mux4x1 - Flow 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Mux4x1 is
    Port ( d0 : in  STD_LOGIC_VECTOR (6 downto 0);
           d1 : in  STD_LOGIC_VECTOR (6 downto 0);
           d2 : in  STD_LOGIC_VECTOR (6 downto 0);
           d3 : in  STD_LOGIC_VECTOR (6 downto 0);
           s0 : in  STD_LOGIC_VECTOR (1 downto 0);
           o0 : out  STD_LOGIC_VECTOR (6 downto 0));
end Mux4x1;

architecture Flow of Mux4x1 is

begin
	with s0 select
		o0 <= d0 when "00",
				d1 when "01",
				d2 when "10",
				d3 when others;

end Flow;
