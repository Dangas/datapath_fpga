--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:27:38 01/25/2017
-- Design Name:   
-- Module Name:   C:/Users/Diego/Desktop/datapath_fpga/test_DataPath.vhd
-- Project Name:  RutaDeDatos
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: DataPath
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_DataPath IS
END test_DataPath;
 
ARCHITECTURE behavior OF test_DataPath IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT DataPath
    PORT(
         clk : IN  std_logic;
         INCP : IN  std_logic;
         ERI : IN  std_logic;
         MX : IN  std_logic_vector(1 downto 0);
         EA : IN  std_logic;
         EB : IN  std_logic;
         UALIn : IN  std_logic_vector(1 downto 0);
         EZ : IN  std_logic;
         CO : OUT  std_logic_vector(1 downto 0);
         FZ : OUT  std_logic;
         memoryO : IN  std_logic_vector(15 downto 0);
         muxZ : OUT  std_logic_vector(6 downto 0);
         ualS : OUT  std_logic_vector(15 downto 0);
         reset : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal INCP : std_logic := '0';
   signal ERI : std_logic := '0';
   signal MX : std_logic_vector(1 downto 0) := (others => '0');
   signal EA : std_logic := '0';
   signal EB : std_logic := '0';
   signal UALIn : std_logic_vector(1 downto 0) := (others => '0');
   signal EZ : std_logic := '0';
   signal memoryO : std_logic_vector(15 downto 0) := (others => '0');
   signal reset : std_logic := '0';

 	--Outputs
   signal CO : std_logic_vector(1 downto 0);
   signal FZ : std_logic;
   signal muxZ : std_logic_vector(6 downto 0);
   signal ualS : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: DataPath PORT MAP (
          clk => clk,
          INCP => INCP,
          ERI => ERI,
          MX => MX,
          EA => EA,
          EB => EB,
          UALIn => UALIn,
          EZ => EZ,
          CO => CO,
          FZ => FZ,
          memoryO => memoryO,
          muxZ => muxZ,
          ualS => ualS,
          reset => reset
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;

		--Caso Aleatorio 1
		memoryO<="1001101010010101";
		ERI<='1';
      MX<="10";
		EA<='1';
		EB<='1';
		INCP<='1';
		EZ<='1';
		UALIn<="01";

		wait for 50 ns;
		assert CO = "10" report "Error case 1 - CO" severity warning;
		assert FZ = '0' report "Error case 1 - FZ" severity warning;
		assert muxZ = "0110101" report "Error case 1 - muxZ" severity warning;
		assert ualS = "0000000000000000" report "Error case 1 - ualS" severity warning;-- modo comparación
		
		--Caso Aleatorio 2
		memoryO<="1111111111111111";
		ERI<='0';
      MX<="11";
		EA<='1';
		EB<='1';
		INCP<='1';
		EZ<='1';
		UALIn<="00";

		wait for 50 ns;
		assert CO = "10" report "Error case 2 - CO" severity warning; --ERI esta desactivado. No se guarda el nuevo valor en el registro
		assert FZ = '1' report "Error case 2 - FZ" severity warning; 
		assert muxZ = "0010101" report "Error case 2 - muxZ" severity warning;--ERI esta desactivado. Valor antiguo. (caso 1)
		assert ualS = "1111111111111110" report "Error case 2 - ualS" severity warning;--suma
		
		
		--Caso Aleatorio 3
		reset<='1';
		memoryO<="1111111111111111";
		ERI<='0';
      MX<="00";
		EA<='1';
		EB<='1';
		INCP<='1';
		EZ<='1';
		UALIn<="10";

		wait for 50 ns;
		assert CO = "00" report "Error case 3 - CO" severity warning; 
		assert FZ = '0' report "Error case 3 - FZ" severity warning;
		assert muxZ = "0000000" report "Error case 3 - muxZ" severity warning;
		assert ualS = "0000000000000000" report "Error case 3 - ualS" severity warning;
		
		

		--Caso Aleatorio 4
		reset<='0';
		memoryO<="1100011111001011";
		ERI<='1';
      MX<="10";
		EA<='1';
		EB<='0';
		INCP<='1';
		EZ<='0';
		UALIn<="01";--modo comparador

		wait for 50 ns;
		assert CO = "11" report "Error case 4 - CO" severity warning; 
		assert FZ = '0' report "Error case 4 - FZ" severity warning;
		assert muxZ = "0001111" report "Error case 4 - muxZ" severity warning;--
		assert ualS /= "0000000000000000" report "Error case 4 - ualS" severity warning; --EA y EB diferentes valores. EB contiene valor antiguo.
		
		--Caso Aleatorio 5
		reset<='0';
		memoryO<="1011111111111111";
		ERI<='0';
      MX<="11";
		EA<='0';
		EB<='0';
		INCP<='0';
		EZ<='0';
		UALIn<="00";

		wait for 50 ns;
		assert CO = "11" report "Error case 5 - CO" severity warning;--ERI desactivado, no se cargar el registro. Valor antiguo. 
		assert FZ = '0' report "Error case 5 - FZ" severity warning;
		assert muxZ = "1001011" report "Error case 5 - muxZ" severity warning;--ERI desactivado, no se cargar el registro. Valor antiguo. 
		assert ualS = "1100011111001011" report "Error case 5 - ualS" severity warning; --1100011111001011 (caso 4) + 0000000000000000 (caso 3, reset activado)


      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
