----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:22:58 01/23/2017 
-- Design Name: 
-- Module Name:    DataPath - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DataPath is
    Port ( clk : in  STD_LOGIC;
           INCP : in  STD_LOGIC;
           ERI : in  STD_LOGIC;
           MX : in  STD_LOGIC_VECTOR (1 downto 0);
           EA : in  STD_LOGIC;
           EB : in  STD_LOGIC;
           UALIn : in  STD_LOGIC_VECTOR (1 downto 0);
           EZ : in  STD_LOGIC;
           CO : out  STD_LOGIC_VECTOR (1 downto 0);
           FZ : out  STD_LOGIC;
           memoryO : in  STD_LOGIC_VECTOR (15 downto 0);
           muxZ : out  STD_LOGIC_VECTOR (6 downto 0);
           ualS : out  STD_LOGIC_VECTOR (15 downto 0);
           reset : in  STD_LOGIC);
end DataPath;



architecture Behavioral of DataPath is

COMPONENT StateReg
	PORT(
		i0 : IN std_logic;
		s0 : IN std_logic;
		clk : IN std_logic;       
		o0 : INOUT std_logic
		);
	END COMPONENT;
	
	COMPONENT UAL
	PORT(
		A : IN std_logic_vector(15 downto 0);
		B : IN std_logic_vector(15 downto 0);
		UAL0 : IN std_logic_vector(1 downto 0);
		CLK : IN std_logic;          
		Z : OUT std_logic;
		S : OUT std_logic_vector(15 downto 0)
		);
	END COMPONENT;
	
	COMPONENT DataRegister
	PORT(
		reset : IN std_logic;
		clk : IN std_logic;
		cs : IN std_logic;
		inputMM : IN std_logic_vector(15 downto 0);          
		outputALU : OUT std_logic_vector(15 downto 0)
		);
	END COMPONENT;
	
	COMPONENT instruction_reg
	PORT(
		ERI : IN std_logic;
		clk : IN std_logic;
		reset : IN std_logic;
		input : IN std_logic_vector(15 downto 0);          
		op : OUT std_logic_vector(1 downto 0);
		addr_src : OUT std_logic_vector(6 downto 0);
		addr_dst : OUT std_logic_vector(6 downto 0)
		);
	END COMPONENT;
	
	COMPONENT Mux4x1
	PORT(
		d0 : IN std_logic_vector(6 downto 0);
		d1 : IN std_logic_vector(6 downto 0);
		d2 : IN std_logic_vector(6 downto 0);
		d3 : IN std_logic_vector(6 downto 0);
		s0 : IN std_logic_vector(1 downto 0);          
		o0 : OUT std_logic_vector(6 downto 0)
		);
	END COMPONENT;
	
	COMPONENT CP
	PORT(
		reset : IN std_logic;
		INCP : IN std_logic;
		E : IN std_logic_vector(6 downto 0);
		clock : IN std_logic;          
		S : OUT std_logic_vector(6 downto 0)
		);
	END COMPONENT;
	
	signal ualZ: std_logic;
	signal ualStoMemory: std_logic_vector(15 downto 0);
	signal regA: std_logic_vector(15 downto 0);
	signal regB: std_logic_vector(15 downto 0);
	signal fzOut: std_logic;
	signal op: std_logic_vector(1 downto 0);
	signal src: std_logic_vector(6 downto 0);
	signal dest: std_logic_vector(6 downto 0);
	signal cpOut: std_logic_vector(6 downto 0);
	signal muxOut: std_logic_vector(6 downto 0);

begin

	StateRegister: StateReg PORT MAP(
		i0 => ualZ,
		s0 => EZ,
		clk => clk,
		o0 => fzOut
	);
	
	FZ<=fzOut;
	
	ALU: UAL PORT MAP(
	A => regA,
	B => regB,
	UAL0 => UALIn,
	CLK => clk,
	Z => ualZ,
	S => ualStoMemory
);

	ualS<=ualStoMemory;

	DataRegisterA: DataRegister PORT MAP(
	reset => reset,
	clk => clk,
	cs => EA,
	inputMM => memoryO,
	outputALU => regA
);

	DataRegisterB: DataRegister PORT MAP(
		reset => reset,
		clk => clk,
		cs => EB,
		inputMM => memoryO,
		outputALU => regB
);

	InstructionRegister: instruction_reg PORT MAP(
			ERI => ERI,
			clk => clk,
			reset => reset,
			input => memoryO,
			op => op,
			addr_src => src,
			addr_dst => dest
);

	CO<=op;
	
	Multiplexor: Mux4x1 PORT MAP(
		d0 => cpOut,
		d1 => (others=>'0'),
		d2 => src,
		d3 => dest,
		s0 => MX,
		o0 => muxOut
	);
	
	muxZ<=muxOut;
	
	ProgramCounter: CP PORT MAP(
		reset => reset,
		INCP => INCP,
		E => muxOut,
		S => cpOut,
		clock => clk
	);
	

end Behavioral;

